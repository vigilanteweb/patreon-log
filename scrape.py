# importing the requests library
import json
import requests
import datetime

today = datetime.date.today()

campaigns = { '2261998', '3236531', '3124832', '126174', '3207772', '3236593', '3236668' }

# api-endpoint
URL = "https://api.patreon.com/campaigns/"

output = {}

for c in campaigns:
    # sending get request and saving the response as response object
    r = requests.get(url = URL + c )

    # extracting data in json format
    data = r.json()

    to_save = {}
    to_save['date'] = today.strftime("%Y-%m-%d")
    to_save['name'] = data['data']['attributes']['name']
    to_save['count'] = data['data']['attributes']['patron_count']
    to_save['pledges'] = data['data']['attributes']['pledge_sum']

    output[c] = to_save

with open('test.json', 'w', encoding='utf-8') as f:
    json.dump(output, f, ensure_ascii=False, indent=4)
#print(output)
